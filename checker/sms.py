from twilio.rest import Client

from checker.models import Color, ProColor, ProStorage, Storage
from checker.settings import settings


def get_pro_message(storage: ProStorage, color: ProColor, max_: bool, url: str) -> str:
    model = "iPhone 15 Pro Max" if max_ else "iPhone 15 Pro"
    return (
        f"{model} {storage}GB in {color.title()} Titanium is available in London. "
        f"Go to url to reserve: {url}"
    )


def get_standard_message(storage: Storage, color: Color, plus: bool, url: str) -> str:
    model = "iPhone 15 Plus" if plus else "iPhone 15"
    return (
        f"{model} {storage}GB in {color.title()} is available in London. "
        f"Go to url to reserve: {url}"
    )


def alert_via_sms(message: str) -> None:
    client = Client(settings.TWILIO_SID, settings.TWILIO_AUTH_TOKEN)

    client.messages.create(to=settings.TO_NUMBER, from_=settings.TWILIO_NUMBER, body=message)
