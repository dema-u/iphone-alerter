import re
from types import TracebackType
from typing import Type

import backoff
import bs4
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.service import Service as FirefoxService
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from typing_extensions import Self
from webdriver_manager.firefox import GeckoDriverManager

from checker.settings import settings

USER_AGENT = (
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 "
    "(KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3"
)

HTML_PARSER = "html.parser"


def store_dropdown_matcher(tag: bs4.Tag) -> bool:
    if tag.has_attr("id"):
        assert isinstance(tag["id"], str)
        if re.match(r"^store.*label$", tag["id"]):
            return True
    return False


class AppleReserveBrowser:
    def __init__(
        self,
        url: str,
        subfamily: str,
        color_no: str,
        storage_str: str,
    ) -> None:
        self._url = url
        self._subfamily = subfamily
        self._color_no = color_no
        self._storage_str = storage_str

    def __enter__(self: Self) -> Self:
        options = Options()

        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument(f"user-agent={USER_AGENT}")

        if settings.HEADLESS_SELENIUM:
            options.add_argument("--headless")
            options.headless = True

        self._driver = webdriver.Firefox(
            options=options, service=FirefoxService(GeckoDriverManager().install())
        )

        return self

    def __exit__(
        self,
        exctype: Type[BaseException] | None,
        excinst: BaseException | None,
        exctb: TracebackType | None,
    ) -> bool:
        self._driver.quit()
        return False

    @backoff.on_exception(
        backoff.constant,
        (TimeoutException),
        interval=settings.PAGE_LOAD_INTERVAL,
        max_tries=settings.PAGE_LOAD_MAX_TRIES,
    )
    def ensure_page_loads(self) -> None:
        self._driver.get(self._url)
        _ = WebDriverWait(self._driver, settings.PAGE_LOAD_TIMEOUT).until(
            EC.presence_of_element_located((By.XPATH, f"//label[@for='{self._subfamily}']"))
        )

    def click_on_options(self) -> None:
        element = self._driver.find_element(By.XPATH, f"//label[@for='{self._subfamily}']")
        element.click()

        element = self._driver.find_element(By.XPATH, f"//label[@for='{self._color_no}']")
        element.click()

        element = self._driver.find_element(By.XPATH, f"//label[@for='{self._storage_str}']")
        element.click()

    def click_on_london_location(self) -> None:
        element = self._driver.find_element(By.XPATH, "//select[@id='anchor-store']")
        option = element.find_element(By.XPATH, "//option[@value='R762']")
        option.click()

    def are_options_available(self) -> bool:
        soup = bs4.BeautifulSoup(self._driver.page_source, HTML_PARSER)
        tag = soup.find("h2", class_="product-title typography-label")
        return True if tag else False

    def is_available_in_london(self) -> bool:
        soup = bs4.BeautifulSoup(self._driver.page_source, HTML_PARSER)
        tags = soup.find_all(store_dropdown_matcher)
        return any([True for t in tags if t.find("span", string="Available") is not None])
