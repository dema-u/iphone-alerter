import logging
from datetime import datetime

import requests
import typer
from typing_extensions import Annotated

from checker.models import (
    get_pro_color_no,
    get_pro_subfamily,
    get_standard_color_no,
    get_standard_subfamily,
    get_storage_str,
)
from checker.settings import settings
from checker.sms import alert_via_sms, get_pro_message, get_standard_message
from checker.state import should_alert, write_alert_state
from checker.web import AppleReserveBrowser

logging.basicConfig(format="")
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def is_available(
    url: str,
    subfamily: str,
    color_no: str,
    storage_str: str,
) -> bool:
    with AppleReserveBrowser(url, subfamily, color_no, storage_str) as browser:
        browser.ensure_page_loads()
        browser.click_on_options()

        options_available = browser.are_options_available()

        if not options_available:
            return False

        browser.click_on_london_location()

        is_available = browser.is_available_in_london()

    return is_available


app = typer.Typer()


@app.command()
def pro(
    storage: Annotated[int, typer.Option()],
    color: Annotated[str, typer.Option()],
    max_: Annotated[bool, typer.Option("--max")] = False,
) -> None:
    url = settings.RESERVE_URL.format("A")

    available = is_available(
        url=url,
        subfamily=get_pro_subfamily(max_),
        color_no=get_pro_color_no(color),
        storage_str=get_storage_str(storage),
    )

    if available and settings.SMS_ENABLED and should_alert():
        alert_via_sms(message=get_pro_message(storage, color, max_, url))
        write_alert_state()

    logger.info(
        f"Checked {storage=}, {color=}, {max_=} at {datetime.utcnow()}. "
        f"Availability: {available}.",
    )


@app.command()
def standard(
    storage: Annotated[int, typer.Option()],
    color: Annotated[str, typer.Option()],
    plus: Annotated[bool, typer.Option("--plus")] = False,
) -> None:
    url = settings.RESERVE_URL.format("F")

    available = is_available(
        url=url,
        subfamily=get_standard_subfamily(plus),
        color_no=get_standard_color_no(color),
        storage_str=get_storage_str(storage),
    )

    if available and settings.SMS_ENABLED and should_alert():
        alert_via_sms(message=get_standard_message(storage, color, plus, url))
        write_alert_state()

    logger.info(
        f"Checked {storage=}, {color=}, {plus=} at {datetime.utcnow()}. "
        f"Availability: {available}."
    )


def natural_15_pro_max_512():
    MODEL_ID = "MU7E3ZD/A"
    DT = datetime.now()

    response = requests.get("https://reserve-prime.apple.com/GB/en_GB/reserve/A/stores.json")

    stores_of_interest = [
        store
        for store in response.json()["stores"]
        if store["city"] in ("London", "Bromley", "Watford")
    ]

    response = requests.get("https://reserve-prime.apple.com/GB/en_GB/reserve/A/availability.json")

    try:
        availability_per_store = response.json()["stores"]
    except requests.exceptions.JSONDecodeError:
        logger.info(f"Apple didn't return a valid json at {DT}.")
        return

    final = {}

    for store in stores_of_interest:
        availability = availability_per_store[store["storeNumber"]][MODEL_ID]["availability"]
        final[store["storeName"]] = availability["contract"] or availability["unlocked"]

    available = any(final.values())

    if available and settings.SMS_ENABLED and should_alert():
        available_loc = ",".join([n for n, a in final.items() if a])
        message = f"Unlocked iPhone 15 Pro Max in Natual Titanium available in {available_loc}"

        alert_via_sms(message=message)
        write_alert_state()

    logger.info(f"Unlocked iPhone 15 Pro Max in Natual Titanium {available=} at {DT}")
