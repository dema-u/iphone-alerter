from typing import Literal

ProStorage = Literal["256", "512", "1024"]
ProColor = Literal["black", "blue", "natural", "white"]

Storage = Literal[128, 256, 512]
Color = Literal["black", "blue", "green", "pink", "yellow"]


def get_standard_subfamily(plus: bool) -> str:
    if plus:
        return "subfamily-iphone-15-plus"
    else:
        return "subfamily-iphone-15"


def get_pro_subfamily(max: bool) -> str:
    if max:
        return "subfamily-iphone-15-pro-max"
    else:
        return "subfamily-iphone-15-pro"


def get_standard_color_no(color: Color) -> str:
    match color:
        case "black":
            color_no = "color-0"
        case "blue":
            color_no = "color-1"
        case "green":
            color_no = "color-2"
        case "pink":
            color_no = "color-3"
        case "yellow":
            color_no = "color-4"
        case _:
            raise RuntimeError(f"Color {color} does not exist.")

    return color_no


def get_pro_color_no(color: ProColor) -> str:
    match color:
        case "black":
            color_no = "color-0"
        case "blue":
            color_no = "color-1"
        case "natural":
            color_no = "color-2"
        case "white":
            color_no = "color-3"
        case _:
            raise RuntimeError(f"Color {color} does not exist.")
    return color_no


def get_storage_str(storage: Storage | ProStorage) -> str:
    match storage:
        case 128:
            storage_str = "capacity-128gb"
        case 256:
            storage_str = "capacity-256gb"
        case 512:
            storage_str = "capacity-512gb"
        case 1024:
            storage_str = "capacity-1tb"
        case _:
            raise RuntimeError(f"Storage size {storage} does not exist.")

    return storage_str
