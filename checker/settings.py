from pathlib import Path

from pydantic import model_validator
from pydantic_settings import BaseSettings, SettingsConfigDict
from typing_extensions import Self


class Settings(BaseSettings):
    HEADLESS_SELENIUM: bool = True

    RESERVE_URL: str = (
        "https://reserve-prime.apple.com/GB/en_GB/reserve/{}/availability?&iUP=E&appleCare=Y"
    )

    PAGE_LOAD_TIMEOUT: int = 4
    PAGE_LOAD_MAX_TRIES: int = 10
    PAGE_LOAD_INTERVAL: int = 0

    SMS_ENABLED: bool = False
    TWILIO_SID: str | None = None
    TWILIO_AUTH_TOKEN: str | None = None
    TWILIO_NUMBER: str | None = None
    TO_NUMBER: str | None = None

    ALERT_STATE_FILE: Path = Path("alert_state.txt")
    ALERT_INTERVAL: int = 1800

    @model_validator(mode="after")
    def validate_sms(self) -> Self:
        if (
            self.SMS_ENABLED
            and not self.TWILIO_SID
            and not self.TWILIO_AUTH_TOKEN
            and not self.TWILIO_NUMBER
            and not self.TO_NUMBER
        ):
            raise RuntimeError(
                "If SMS alerts are enabled the fields TWILIO_SID, "
                "TWILIO_AUTH_TOKEN, TWILIO_NUMBER and TO_NUMBER have to be set."
            )
        return self

    model_config = SettingsConfigDict(env_file=".env", env_file_encoding="utf-8")


settings = Settings()
