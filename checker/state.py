from datetime import datetime

from checker.settings import settings


def write_alert_state() -> None:
    with open(settings.ALERT_STATE_FILE, "w") as f:
        f.write(datetime.now().isoformat())


def should_alert() -> bool:
    if not settings.ALERT_STATE_FILE.exists():
        return True

    with open(settings.ALERT_STATE_FILE, "r") as f:
        timestamp = f.read().strip()

    last_alert_time = datetime.fromisoformat(timestamp)
    delta = datetime.now() - last_alert_time

    return delta.seconds >= settings.ALERT_INTERVAL
